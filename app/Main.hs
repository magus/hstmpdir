module Main where

import System.Unix.Directory
import System.IO
import System.FilePath

main :: IO ()
main = withTemporaryDirectory "foobar" doStuff

doStuff p = do
  withFile (p </> "tmpFile") WriteMode $ \ h -> do
    hPutStrLn h "foobarbaz"
  readFile (p </> "tmpFile") >>= putStr
  putStrLn "done"
